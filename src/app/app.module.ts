//import { BrowserModule } from '@angular/platform-browser';

import { WorkerAppModule } from '@angular/platform-webworker';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        WorkerAppModule
        //BrowserModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
